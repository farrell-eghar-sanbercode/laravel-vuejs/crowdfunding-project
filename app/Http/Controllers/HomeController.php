<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function verifEmail() {
        return 'Halaman ini dapat diakses oleh User dan Admin yang terverifikasi';
    }

    public function adminDanVerifEmail() {
        return 'Anda adalah Admin yang terverifikasi';
    }
}
