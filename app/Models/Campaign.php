<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;

class Campaign extends Model
{
    use Uuid;

    protected $table = 'campaigns';

    protected $guarded = [];
}
