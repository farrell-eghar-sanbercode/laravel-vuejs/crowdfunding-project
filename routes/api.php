<?php

// use Illuminate\Http\Request;

// /*
// |--------------------------------------------------------------------------
// | API Routes
// |--------------------------------------------------------------------------
// |
// | Here is where you can register API routes for your application. These
// | routes are loaded by the RouteServiceProvider within a group which
// | is assigned the "api" middleware group. Enjoy building your API!
// |
// */

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::namespace('Auth')->prefix('auth')->group(function() {
    Route::post('register', 'RegisterController');
    Route::post('login', 'LoginController');
    
    Route::get('/social/{provider}', 'SocialiteController@redirectToProvider');
    Route::get('/social/{provider}/callback', 'SocialiteController@handleProviderCallback');
});

Route::namespace('Auth')->middleware('auth:api')->prefix('auth')->group(function() {
    Route::post('verification', 'VerificationController');
    Route::post('regen-otp', 'RegenerateOtpCodeController');
    Route::post('logout', 'LogoutController');
    Route::post('check-token', 'CheckTokenController');
});

Route::middleware('auth:api')->prefix('profile')->group(function() {
    Route::get('user', 'UserController@index');
    Route::post('update-profile', 'UserController@update');
});

Route::prefix('campaign')->group(function() {
    Route::get('random/{count}', 'CampaignController@random');
    Route::post('store', 'CampaignController@store');
    Route::get('/', 'CampaignController@index');
    Route::get('/{id}', 'CampaignController@detail');
    Route::get('search/{keyword}', 'CampaignController@search');
});

Route::prefix('blog')->group(function() {
    Route::get('random/{count}', 'BlogController@random');
    Route::post('store', 'BlogController@store');
});
